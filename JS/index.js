const DSSV_LOCALSTORAGE = "DSSV_LOCALSTORAGE";

// Chức năng thêm sinh viên
var dssv = [];
var editableMode = false;

// Lấy thông tin từ localStorage
var dssvJson = localStorage.getItem(DSSV_LOCALSTORAGE);
if (dssvJson != null) {
  dssv = JSON.parse(dssvJson);
  // array khi convert thành json sẽ mất funtion, ta sẽ map lại
  for (var index = 0; index < dssv.length; index++) {
    sv = dssv[index];

    dssv[index] = new SinhVien(
      sv.ma,
      sv.ten,
      sv.email,
      sv.matKhau,
      sv.toan,
      sv.ly,
      sv.hoa
    );
  }

  renderDSSV(dssv);
}

function themSV() {
  var newSv = layThongTinTuForm();
  //   console.log("newSv", newSv);

  // kiểm tra mã sinh viên
  var isValid =
    validator.kiemTraRong(
      newSv.ma,
      "spanMaSV",
      "Mã sinh viên không được để trống"
    ) &&
    validator.kiemTraDoDai(
      newSv.ma,
      "spanMaSV",
      "Mã sinh viên phải gồm 4 kí tự",
      4,
      4
    );

  // kiểm tra tên sinh viên
  var isValid =
    isValid &
    validator.kiemTraRong(
      newSv.ten,
      "spanTenSV",
      "Tên sinh viên không được để trống"
    );

  // kiểm tra email sinh vien
  var isValid =
    isValid &
    validator.kiemTraEmail(
      newSv.email,
      "spanEmailSV",
      "Email phải là @gmail.com"
    );

  if (isValid) {
    dssv.push(newSv);
    // tạo json
    var dssvJson = JSON.stringify(dssv);
    // lưu json vào localStorage
    localStorage.setItem(DSSV_LOCALSTORAGE, dssvJson);
    renderDSSV(dssv);
  }
}

function reset() {
  document.getElementById("txtMaSV").value = "";
  document.getElementById("txtTenSV").value = "";
  document.getElementById("txtEmail").value = "";
  document.getElementById("txtPass").value = "";
  document.getElementById("txtDiemToan").value = "";
  document.getElementById("txtDiemLy").value = "";
  document.getElementById("txtDiemHoa").value = "";
}

function capNhat() {
  var maSv = document.getElementById("txtMaSV").value;
  if (maSv.length > 0 && editableMode) {
    var index = dssv.findIndex(function (sv) {
      return sv.ma == maSv;
    });

    var sv = dssv[index];

    sv.ma = document.getElementById("txtMaSV").value;
    sv.ten = document.getElementById("txtTenSV").value;
    sv.email = document.getElementById("txtEmail").value;
    sv.matKhau = document.getElementById("txtPass").value;
    sv.toan = document.getElementById("txtDiemToan").value;
    sv.ly = document.getElementById("txtDiemLy").value;
    sv.hoa = document.getElementById("txtDiemHoa").value;

    dssv[index] = sv;
    editableMode = false;
    reset();
  }

  var dssvJson = JSON.stringify(dssv);
  // lưu json vào localStorage
  localStorage.setItem(DSSV_LOCALSTORAGE, dssvJson);
  renderDSSV(dssv);
}

function xoaSinhVien(id) {
  console.log(id);
  var index = dssv.findIndex(function (sv) {
    return sv.ma == id;
  });
  console.log(index);
  dssv.splice(index, 1);
  renderDSSV(dssv);
}

function timKiemSinhVien() {
  var searchKey = document.getElementById("txtSearch").value;

  var dssvFiltered = dssv.filter(function (sv) {
    return sv.ten.includes(searchKey);
  });
  renderDSSV(dssvFiltered);
}

function suaSinhVien(id) {
  // var index = timKiemViTri(id, dssv);
  var index = dssv.findIndex(function (sv) {
    return sv.ma == id;
  });
  console.log("index", index);
  if (index != -1) {
    var sv = dssv[index];
    showThongTinLenForm(sv);
    editableMode = true;
  }
}
