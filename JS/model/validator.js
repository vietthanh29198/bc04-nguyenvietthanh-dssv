var validator = {
  kiemTraRong: function (value, idError, message) {
    if (value.length == 0) {
      document.getElementById(idError).innerHTML = message;
      return false;
    } else {
      document.getElementById(idError).innerHTML = "";
      return true;
    }
  },

  kiemTraDoDai: function (value, idError, message, min, max) {
    if (value.length < min || value.length > max) {
      document.getElementById(idError).innerHTML = message;
      return false;
    } else {
      document.getElementById(idError).innerHTML = "";
      return true;
    }
  },

  kiemTraEmail: function (value, idError, message) {
    const re =
      /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

    if (re.test(value)) {
      document.getElementById(idError).innerHTML = "";
      return true;
    } else {
      document.getElementById(idError).innerHTML = message;
      return false;
    }
  },
};
// regex email js
